# README #

NyanFighters - a game for Minecraft

### What is this repository for? ###

This Repository is the plugin Nyanfighters, which is a game, coded with the BukkitAPI for the game minecraft. You can play this game soon on miningplayers.net.
Nyanfighters is quite simple. You fight, kill and win (as long as you have the most kills). But the normal Player vs Player mode is to boring, so this adds more features and a fun movement. You run on a rainbow in sky and have some special-items to use:

1. a granate which explodes on impact

2. a trap which spawns many webs around the player

3. a enderpearl which ports you where you looking

4. a pusher which pushes all players from you away

5. a slimeblock which gives you strength to you legs

6. a stonesword for attack

7. a fishing hook for fun

8. a full diamond armor for best protect

If you jump in air, a rainbow will build under you, so that you can jump up. If you walk around, a rainbow is your way. If you look down, the rainbow will let you go down fast but save.

### How do I get set up? ###

You must have an minecraft-server on version 1.9 or higher. Put the jar, which must be build from the guide, in to the plugins folder of your server.
Now go ingame on your server and set the following coordinates:

1. Set your LobbySpawn with /setspawn Lobby

2. Set your game spawns with /setspawn Spiel <number>

3. Set your arena to fight with /setspawn pos <1/2>


Attention: Where you looking at, when you set the spawn, the players will look, when they spawns there. To see the spawns use /spawn <Lobby/Spiel> [number]

If you haven't done this commands, many buggs will appear. Also make the server restart after every game (can be included in the plugin).


### Who do I talk to? ###

My name is paul and I'm a developer on miningplayers.net where you can find this game too. 

I hope you enjoy this game :)